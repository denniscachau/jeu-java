package model;

/**
 * <h1>The Class Enemy</h1>
 *
 * @author Dennis Cachau
 * @version 1.0
 */
public class Enemy extends CharacterImpl {
    /**
     * The enemy constructor
     *
     * @param position
     */
	public Enemy(Position position) {
		super (Direction.NOTHING, position, "sprite\\monster_1.png");
	}
}
