package model;

/**
 * <h1>The Class Hero</h1>
 *
 * @author Dennis Cachau
 * @version 1.0
 */
public class Hero extends CharacterImpl {
	
    /**
     * The hero constructor
     *
     * @param direction, position, imageURL
     */
	public Hero(Direction direction, Position position, String imageURL) {
		super(direction, position, "sprite\\lorann_u.png");

	}

    /**
     * Encounters of the hero.
     *
     * @param element
     *            
     * @return what the spell does depending on what it encounters (the param)
     */
	public void encounter(LevelElement element) {

		switch (element) {
		case Enemy:
			System.exit(0);
			break;
		case Spell:
			haveSpell(true);
			break;
		case Gate:
			System.out.println("Win");
			System.exit(0);
			break;
		case Snake:
			System.out.println("Game Over");
			System.exit(0);
			break;
		case EnergyBubble:
			new Gate(this.getPosition());
			break;
		case HBone:
			switch (getDirection()) {
			case UP:
				move(Direction.DOWN);
				break;
			case DOWN:
				move(Direction.UP);
				break;
			default:
				break;
			}
			break;

		case VBone:
			switch (getDirection()) {
			case LEFT:
				move(Direction.RIGHT);
				break;
			case RIGHT:
				move(Direction.LEFT);
				break;
			default:
				break;
			}
		default:
			break;
		}
	}

    /**
     * Manage if the hero has a spell or not
     *
     * @param spell
     *            
     * @return true if the hero has the spell
     */
	public boolean haveSpell(boolean spell) {
		return spell;
	}

}
