/**
 * Provides all classes for the DAO.
 *
 * @author Th�o De Ana
 * @version 1.0
 */
package model.dao;
