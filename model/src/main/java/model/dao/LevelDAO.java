package model.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Bone;
import model.Direction;
import model.Element;
import model.Empty;
import model.Enemy;
import model.EnergyBubble;
import model.Gate;
import model.GoldPocket;
import model.HBone;
import model.Hero;
import model.Level;
import model.LevelElement;
import model.LevelImpl;
import model.Position;
import model.PositionImpl;
import model.Snake;
import model.Spell;
import model.VBone;

/**
 * <h1>The Class LevelDAO.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class LevelDAO {

    /** The sql level by level id. */
    private static String sqlLoadLevelById  = "{call sp_loadLevelById(?)}";

    /** The different columns index used. */
    private static String objectTypeColumnIndex = "objectType";
    private static String posXColumnIndex       = "posX";
    private static String posYColumnIndex		= "posY";

    /**
     * Gets the level by id.
     *
     * @param id
     *            the id
     * @return the level
     * @throws SQLException
     *             the SQL exception
     */
    public Level getLevelById(final int id) throws SQLException {
    	Level level = new LevelImpl();
        
    	final CallableStatement callStatement = prepareCall(sqlLoadLevelById);
        callStatement.setInt(1, id);
        if (callStatement.execute()) {
            final ResultSet result = callStatement.getResultSet();
        while (result.next()) {
            String typeStr = result.getString(objectTypeColumnIndex);
            int x = result.getInt(posXColumnIndex);
            int y = result.getInt(posYColumnIndex);
            Position position = new PositionImpl(x,y);
            Element element;
            LevelElement levelElement = LevelElement.valueOf(typeStr);
            for (int row = 0; row<12; row++) {
            	for (int col = 0; col<20; col++) {
            		Position positionEmpty = new PositionImpl(row, col);
            		level.addElement(new Empty(positionEmpty));
            	}
            }
            
            level.removeElement(level.getElementByPosition(position));
            
            switch(levelElement) {
            case HBone :
            	element = new HBone(position); 
            	break;
            	
            case VBone :
            	element = new VBone(position);
            	break;
            	
            case Snake :
            	element = new Snake(position);
            	break;
            	
            case Bone :
            	element = new Bone(position); 
            	break;
            	
            case Enemy :
            	element = new Enemy(position);   
            	break;
            	
            case GoldPocket :
            	element = new GoldPocket(position);
            	break;
            	
            case Hero :
            	element = new Hero(Direction.UP, position, typeStr);
            	break;
            	
            case EnergyBubble :
            	element = new EnergyBubble(position);
            	break;
            
            case Gate : 
            	element = new Gate(position);
            	break;
            	
            case Spell : 
            	element = new Spell(Direction.NOTHING, position, typeStr);
            	break;
            	
            default :
            	element = new Empty(position);
            	break;
            }
            level.addElement(element);
        }
        
            result.close();
        }
        return level;
    }

    /**
     * Prepares the stored procedure call in the instance
     *
     * @param query
     *            the query you want to call
     * @return the call in the instance
     */
    protected static CallableStatement prepareCall(final String query) {
        return LorannBDDConnector.getInstance().prepareCall(query);
    }

}
