/**
 * Provides all classes for the controller component.
 * 
 * @author Th�o De Ana & Dennis Cachau
 * @version 1.0
 */
package model;
