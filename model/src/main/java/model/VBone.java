package model;

/**
 * <h1>The Class VBone.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class VBone extends ElementImpl {
    /**
     * The vbone constructor
     *
     * @param position
     */
	public VBone(Position position) {
		super(position, "sprite\\vertical_bone.png");
	}
}
