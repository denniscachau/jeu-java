package model;

/**
 * <h1>The Class Bone.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class Bone extends ElementImpl {
    /**
     * The bone constructor
     *
     * @param position
     */
		public Bone(Position position) {
			super(position, "sprite\\bone.png");
		}
}
