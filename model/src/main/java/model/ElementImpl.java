package model;

import java.awt.Image;

/**
 * <h1>The Class ElementImpl</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */

import javax.swing.ImageIcon;
/**
 * <h1>The Class ElementImpl.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class ElementImpl implements Element {
	protected Image img;
	protected  Position position = null;
	
	/**
	 * The constructor ElementImpl
	 *
	 * @param position, image
	 */
	public ElementImpl(Position position, Image image) {
		this.img = image;
		this.position = position;
	}
	/**
	 * The constructor ElementImpl
	 *
	 * @param position, image
	 */
	public ElementImpl(Position position, String imageURL) {
		this.position = position;
		loadImage(imageURL);
	}
	
	/**
	 * Gets the position
	 * 
	 * @return the position
	 */
	@Override
	public Position getPosition() {
		return position;
	}

	/**
	 * Loads the image
	 * 
	 * @param imageURL
	 */
	@Override
	public void loadImage(String imageURL) {
		try {
			ImageIcon img = new ImageIcon(imageURL);
			//img = ImageIO.read(new File (imageURL));
			this.img = img.getImage();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	/**
	 * Gets the image
	 * 
	 * @return img
	 */
	@Override
	public Image getImage() {
		return img;
	}
}
