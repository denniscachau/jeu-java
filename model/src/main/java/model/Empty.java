package model;

/**
 * <h1>The Class Empty.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class Empty extends ElementImpl {
    /**
     * The empty constructor
     *
     * @param position
     */
	public Empty(Position position) {
		super(position, "sprite\\empty.png");
	}
}
