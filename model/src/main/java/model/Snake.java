package model;
/**
 * <h1>The Class Snake.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class Snake extends ElementImpl {
    /**
     * The Snake constructor
     *
     * @param position
     */
	public Snake(Position position) {
		super(position, "sprite\\gate_closed.png");
	}
}
