package model;

/**
 * <h1>The Class Spell</h1>
 *
 * @author Dennis Cachau
 * @version 1.0
 */
public class Spell extends CharacterImpl {

    /**
     * The spell constructor
     *
     * @param direction, position, imageURL
     */
	public Spell(Direction direction, Position position, String imageURL) {
		super(direction, position, "sprite\\fireball_2.png");
	}

    /**
     * Encounters of the spell.
     *
     * @param element
     *            
     * @return what the spell does depending on what it encounters (the param)
     */
	public void encounter(LevelElement element) {
		switch (element) {
		case HBone:
			switch (getDirection()) {
			case UP:
				move(Direction.DOWN);
				break;
			case DOWN:
				move(Direction.UP);
				break;
			default:
				break;
			}
			break;
		
		case VBone :
			switch (getDirection()) {
			case LEFT:
				moveRight();
				break;
			case RIGHT:
				moveLeft();
				break;
			default:
				break;
			}
		default:
			break;
		}
	}
}
