package model;

/**
 * <h1>The class CharacterImpl</h1>
 *
 * @author Dennis Cachau
 * @version 1.0
 */
import java.awt.Image;

import javax.swing.ImageIcon;


public class CharacterImpl extends ElementImpl implements Character {
	
	/** The position. */
	private Position position;
	/** The direction. */
	private Direction direction;
	/** The img. */
	protected Image img;
	/** The boolean moved. */
	private boolean moved;

    /**
     * The characterimpl constructor
     *
     * @param direction, position, imageURL
     */
	public CharacterImpl(Direction direction, Position position, String imageURL) {
		super(position, imageURL);
		this.direction = getDirection();
	}
	
	/**
     * Gets the direction.
     *            
     * @return direction
     */
	@Override
	public Direction getDirection() {
		return direction;
	}
	
	/**
     * Gets the position.
     *            
     * @return position
     */
	@Override
	public Position getPosition() {
		return position;
	}

	/**
     * Loads the image.
     * 
     * @param imageURL
     *            
     * @return the image loads
     */
	@Override
	public void loadImage(String imageURL) {
		try {
			ImageIcon img = new ImageIcon(imageURL);
			//img = ImageIO.read(new File (imageURL));
			this.img = img.getImage();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	/**
     * Gets if the hero have a spell or not.
     *            
     * @param element
     *            
     * @return element
     */
	public boolean haveSpell(boolean element) {
		return element;
	}

	/**
     * Encounter.
     *            
     * @param element
     */
	@Override
	public void encounter(LevelElement element) {
		// TODO Auto-generated method stub
	}

	/**
     * Gets if an element moved or not.
     *            
     * @return true if an element moved
     */
	@Override
	public boolean hasMoved() {
		// TODO Auto-generated method stub
		return moved;
	}

	/**
     * Sets if an elements moved or not.
     *            
     * @return moved
     * 			   the new moved
     */
	@Override
	public void setMoved(boolean moved) {
		// TODO Auto-generated method stub
		this.moved = moved;
	}
	
	/**
     * Manages the movement of the elements.
     *            
     * @param direction
     *            
     * @return execute methods in function of the direction
     */
	@Override
	public void move(Direction direction) {
		switch (direction) {
			case LEFT 		:  this.moveLeft();  	 break;
			case RIGHT		:  this.moveRight();  	 break;
			case DOWN 		:  this.moveDown();  	 break;
			case UP   		:  this.moveUp();  		 break;
			case UPRIGHT   	:  this.moveUpRight();	 break;
			case UPLEFT   	:  this.moveUpLeft();  	 break;
			case DOWNRIGHT  :  this.moveDownRight(); break;
			case DOWNLEFT   :  this.moveUpLeft();  	 break;
			case NOTHING 	:  this.moveNothing(); 	 break;
			default			:  break;
		}
		setMoved(true);
	}
	
	
//	@Override
	public void moveUp() {
		
		this.getPosition().setY(this.getPosition().getY() - 1);
		this.getPosition().setX(this.getPosition().getX());
	}

//	@Override
	public void moveRight() {
		this.position.setY(position.getY());
		this.position.setX(position.getX() + 1);
	}

//	@Override
	public void moveDown() {
		this.position.setY(position.getY() + 1);
		this.position.setX(position.getX());
	}

//	@Override
	public void moveLeft() {
		this.position.setY(position.getY());
		this.position.setX(position.getX() - 1);
	}

//	@Override
	public void moveUpRight() {
		this.position.setY(position.getY() - 1);
		this.position.setX(position.getX() + 1);
	}

//	@Override
	public void moveUpLeft() {
		this.position.setY(position.getY() - 1);
		this.position.setX(position.getX() - 1);
	}

//	@Override
	public void moveDownRight() {
		this.position.setY(position.getY() + 1);
		this.position.setX(position.getX() + 1);
	}

//	@Override
	public void moveDownLeft() {
		this.position.setY(position.getY() + 1);
		this.position.setX(position.getX() - 1);
	}

//	@Override
	public void moveNothing() {
		this.position.setY(position.getY());
		this.position.setX(position.getX());
	}

}
