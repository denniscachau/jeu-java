package model;

/**
 * <h1>The Class PositionImpl.</h1>
 *
 * @author Th�o De Ana & Dennis Cachau
 * @version 1.0
 */
public class PositionImpl implements Position {

	/** The integers used to create a position*/
	private int x;
	private int y;
	
	/** Instantiate a new Position*/
	public PositionImpl(int x, int y) {
		this.x    = x;
		this.y    = y;
	}

	/* 
	 * Getters and setters.
	 */
	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * The methods used to compare a position from the database and a position from the grid.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PositionImpl other = (PositionImpl) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	
}
