package model;

/**
 * <h1>The Class GoldPocket.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class GoldPocket extends ElementImpl {
    /**
     * The goldpocket constructor
     *
     * @param position
     */
	public GoldPocket(Position position) {
		super(position, "sprite\\purse.png");
	}
}
