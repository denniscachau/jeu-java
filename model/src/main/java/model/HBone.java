package model;

/**
 * <h1>The Class HBone.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class HBone extends ElementImpl {
    /**
     * The hbone constructor
     *
     * @param position
     */
	public HBone(Position position) {
		super(position, "sprite\\horizontal_bone.png");
	}
}
