package model;

/**
 * <h1>The Class Gate.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class Gate extends ElementImpl {
    /**
     * The gate constructor
     *
     * @param position
     */
	public Gate(Position position) {
		super(position, "sprite\\gate_open.png");
	}
}
