package model;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>The Class LevelImpl provides a facade of the Model component.</h1>
 *
 * @author Dennis Cachau
 * @version 1.0
 */
public final class LevelImpl implements Level {
	
	private List<Element> elements = new ArrayList<>();
	private Character lorann;
	private LevelObserver observer;
	
    /**
     * Instantiates a new level.
     */
    public LevelImpl() {
        super();
    }
    
    /**
     * Manages the collisions of elements.
     *
     * @param hero, gate, snake
     *            
     * @return the encounter with the element
     */
    
    public void manageEncounter(Character hero, Element gate, Element snake) {

        // isSpellOnEnnemy

        // isSpellOnHero


        if(hero.getPosition().equals(gate.getPosition())){ // isHeroOnGate
             hero.encounter(LevelElement.Gate);
         }

        if(hero.getPosition().equals(snake.getPosition())){ // isHeroOnSnake
             hero.encounter(LevelElement.Snake);
         }

    }
    
    /**
     * Adds a new element to the list elements.
     *
     * @param element
     *            
     * @return the list with a new element
     */
	@Override
	public void addElement(Element element) {	
		elements.add(element);
	}
	
	/**
     * Gets the element in this position.
     *
     * @param position
     *            
     * @return the element
     */
	@Override
	public Element getElementByPosition(Position position){
		for(Element element : elements) {
			if (position.equals(element.getPosition())) {
				return element;
			}
		}
		throw new RuntimeException("Element not found in list");
	}

	/**
     * Removes an element in the list elements.
     *
     * @param element
     *            
     * @return the list with an element less
     */
	@Override
	public void removeElement(Element element) {
		for (Element toRemove : elements) {
			if (toRemove.getPosition().equals(element.getPosition())) {
				elements.remove(toRemove);
			}
		}
	}

	/**
     * Gets Lorann element.
     *            
     * @return lorann
     */
	@Override
	public Character getLorann() {
		return lorann;
	}

	/**
     * Adds Lorann to the list.
     *
     * @param character
     *            
     * @return the element
     */
	@Override
	public void addLorann(Character character) {
		lorann = character;
	}

	/**
     * Adds the observer.
     *
     * @param observer
     *            the new observer
     */
	@Override
	public void addObserver(LevelObserver observer) {
		this.observer = observer;
	}

	/**
     * Notifies the observer.
     *
     * @return the change to view
     */
	@Override
	public void notifyObserver() {
		observer.onChange();
	}

//	@Override
//	public List<Character> getEnemies() {
//		List<Character> ennemies = new ArrayList<>();
//		for (Element element : elements) {
//			
//		}
//		return ennemies;
//	}
	
	/**
     * Converts to String
     *
     * @return a String
     */
	@Override
	public String toString() {
		String out = "";
		for (int row = 0; row < 12; row++) {
			for (int col = 0; col < 20; col++) {
				try {
					Element modelElement = getElementByPosition(new PositionImpl(col, row));
					out += modelElement.getClass().getSimpleName().substring(0,1);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			out += "\n";
		}
		return out;
	}
}
