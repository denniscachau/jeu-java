package model;
/**
 * <h1>The Class EnergyBubble.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public class EnergyBubble extends ElementImpl {
    /**
     * The bone constructor
     *
     * @param position
     */
	public EnergyBubble(Position position) {
		super(position, "sprite\\crystal_ball.png");
	}
}
