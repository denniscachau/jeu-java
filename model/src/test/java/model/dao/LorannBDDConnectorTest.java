package model.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LorannBDDConnectorTest {
	
	private static String user = "root";
	private static String password = "";
	private static String url = "jdbc:mysql://localhost/lorann?useSSL=false&serverTimezone=UTC";
	      
	
	private static String userTest = "root";
	private static String passwordTest = "";
	private static String urlTest = "jdbc:mysql://localhost/lorann?useSSL=false&serverTimezone=UTC";
	
	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void testGetInstance() {
		fail("Not yet implemented");
	}

	//@Test
	public void testExecuteQuery() {
		fail("Not yet implemented");
	}

	//@Test
	public void testPrepareCall() {
		try {
			Connection connection = DriverManager.getConnection(LorannBDDConnectorTest.url, LorannBDDConnectorTest.user,LorannBDDConnectorTest.password);
			CallableStatement callableStatement = connection.prepareCall("{call sp_chargeLevel(1)}");
			callableStatement.execute();
			ResultSet resultSet;
			assertEquals(true, resultSet = callableStatement.getResultSet());
		}catch (Exception e) {
			fail("Can't execute the call");
		}
		
	}

	//@Test
	public void testExecuteUpdate() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testGetConnection() throws SQLException {
		try {
			final Connection connection = DriverManager.getConnection(LorannBDDConnectorTest.url, LorannBDDConnectorTest.user,LorannBDDConnectorTest.password);
			assertEquals(true, connection.isValid(1000));
		} catch (Exception e) {
			fail("Can't connect to the DB");
		}
	}
	
	@Test
	public void testSetConnection() throws Exception{
		try {
			final Connection connection = DriverManager.getConnection(LorannBDDConnectorTest.url, LorannBDDConnectorTest.user,LorannBDDConnectorTest.password);
			final Connection connectionTest = DriverManager.getConnection(LorannBDDConnectorTest.urlTest, LorannBDDConnectorTest.userTest,LorannBDDConnectorTest.passwordTest);
			assertEquals(connection.isValid(1000), connectionTest.isValid(1000));
		}
		catch (Exception e) {
			fail("Couldn't set the connection");
		}
	}
	

	//@Test
	public void testGetStatement() {
		fail("Not yet implemented");
	}

	//@Test
	public void testSetStatement() {
		fail("Not yet implemented");
	}

}
