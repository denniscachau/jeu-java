/**
 * Provides all classes for the controller component.
 *
 * @author Dennis Cachau
 * @version 1.0
 */
package controller;