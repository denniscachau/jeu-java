package controller;

import java.sql.SQLException;

import model.Direction;
import model.Level;
import view.ViewFacade;

/**
 * <h1>The Class ControllerFacadeImpl provides a facade of the Controller component.</h1>
 *
 * @author Dennis CACHAU
 * @version 1.0
 */
public class ControllerFacadeImpl implements ControllerFacade {

    /** The view. */
    private ViewFacade view;
//    private Position position;

    public void setView(ViewFacade view) {
		this.view = view;
		level.addObserver(view);
	}

	/** The level. */
    private final Level level;

    /**
     * Instantiates a new controller facade.
     *
     * @param view
     *            the view
     * @param level
     *            the model
     */
    public ControllerFacadeImpl(Level level) {
        this.level = level;
    }

    /**
     * Start.
     *
     * @throws SQLException
     *             the SQL exception
     */
    public void start() throws SQLException {       
        //this.getView().displayLevel(level);
    }

    /**
     * Gets the view.
     *
     * @return the view
     */
    public ViewFacade getView() {
        return this.view;
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public Level getLevel() {
        return this.level;
    }
    /**
     * Sends the direction of Lorann to the view
     *
     * @return informations to viewFacade
     */
	public void orderPerform(Direction direction) {
		level.getLorann().move(direction);
		level.notifyObserver();
		
	}
	
	
}
