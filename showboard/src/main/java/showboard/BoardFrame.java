package showboard;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.Observer;

/**
 * <h1>The Interface IBoard.</h1>
 *
 * @author Anne-Emilie DIET
 * @version 1.0
 * @see Dimension
 * @see Rectangle
 * @see Square
 * @see Pawn
 */
public interface BoardFrame {

    /**
     * Adds the square.
     *
     * @param square
     *            the square
     * @param x
     *            the x
     * @param y
     *            the y
     */
    void addSquare(Square square, int x, int y);

    /**
     * Adds the pawn.
     *
     * @param pawn
     *            the pawn
     */
    void addPawn(Pawn pawn);

    /**
     * Gets the observer.
     *
     * @return the observer
     */
    Observer getObserver();

    /**
     * Sets the dimension.
     *
     * @param dimension
     *            the new dimension
     */
    void setDimension(Dimension dimension);

    /**
     * Gets the dimension.
     *
     * @return the dimension
     */
    Dimension getDimension();

    /**
     * Sets the display frame.
     *
     * @param displayFrame
     *            the new display frame
     */
    void setDisplayFrame(Rectangle displayFrame);
    
    void update();
}
