package showboard;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;


/**
 * <h1>The Class BoardFrame.</h1>
 * <p>
 * This class is just used to load the BoardPanel. It extends JPanel and implements IBoard.
 * </p>
 * <p>
 * As the BoardPanel is a private class, BoardPanel is a Facade.
 * </p>
 *
 * @author Vladimir Camalot & Lucas Suberbielle
 * @version 3.0
 * @see JFrame
 * @see BoardPanel
 * @see Dimension
 * @see Rectangle
 * @see BoardFrame
 * @see Square
 * @see Pawn
 */
public class BoardFrameImpl extends JFrame implements BoardFrame {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6563585351564617603L;

    /** The initial frame size. */
    private static final int  defaultWidthFrameSize = 640;
    private static final int  defaultHeightFrameSize = 384;

    /** The board panel. */
    private final BoardPanel  boardPanel;
    
    /**The dimension.*/
    private Dimension dimension;

    /**
     * Instantiates a new board frame.
     *
     * @param title
     *            the title of the frame
     * @param decorated
     *            the decorated
     */
    public BoardFrameImpl(final String title, final Boolean decorated) {
        super();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.boardPanel = new BoardPanel();
        this.setContentPane(this.boardPanel);
        JFrame boardPanel = new JFrame();
		JPanel pan = new JPanel(new GridLayout(12, 20));
		Border blackline = BorderFactory.createLineBorder(Color.black, 1);
		for (int i = 0; i < 12; i++) {
			for (int j = 0; j < 20; j++) {
				JPanel ptest = new JPanel();
				ptest.setBorder(blackline);
				boardPanel.setTitle(title);
				boardPanel.setUndecorated(decorated);
				boardPanel.setResizable(false);
				boardPanel.setLocationRelativeTo(null);
				boardPanel.setSize(defaultWidthFrameSize, defaultHeightFrameSize);
				pan.add(ptest);
			}
		}
		pan.setBorder(blackline);
		boardPanel.add(pan);
		boardPanel.setVisible(true);
    }

    /**
     * Instantiates a new board frame.
     *
     * @param title
     *            the title
     */
    public BoardFrameImpl(final String title) {
        this(title, false);
    }

    /**
     * Instantiates a new board frame.
     */
    public BoardFrameImpl() {
        this("", false);
    }

    /**
     * Instantiates a new board frame.
     *
     * @param decorated
     *            the decorated
     */
    public BoardFrameImpl(final Boolean decorated) {
        this("", decorated);
    }

    /*
     * (non-Javadoc)
     * @see fr.exia.showboard.IBoard#addSquare(fr.exia.showboard.ISquare, int, int)
     */
    @Override
    public final void addSquare(final Square square, final int x, final int y) {
        this.getBoardPanel().addSquare(square, x, y);
    }

    /*
     * (non-Javadoc)
     * @see fr.exia.showboard.IBoard#addPawn(fr.exia.showboard.IPawn)
     */
    @Override
    public final void addPawn(final Pawn pawn) {
        this.getBoardPanel().addPawn(pawn);
    }

    /*
     * (non-Javadoc)
     * @see fr.exia.showboard.IBoard#getObserver()
     */
    @Override
    public final Observer getObserver() {
        return this.getBoardPanel();
    }

    /*
     * (non-Javadoc)
     * @see fr.exia.showboard.IBoard#setDimension(java.awt.Dimension)
     */
    @Override
    public final void setDimension(final Dimension dimension) {
        this.getBoardPanel().setDimension(dimension);
    }

    /*
     * (non-Javadoc)
     * @see fr.exia.showboard.IBoard#getDimension()
    @Override
    public final Dimension getDimension() {
        return this.getBoardPanel().getDimension();
    }

    /**
     * Gets the display frame.
     *
     * @return the display frame
     */
    public final Rectangle getDisplayFrame() {
        return this.getBoardPanel().getDisplayFrame();
    }

    /*
     * (non-Javadoc)
     * @see fr.exia.showboard.IBoard#setDisplayFrame(java.awt.Rectangle)
     */
    @Override
    public final void setDisplayFrame(final Rectangle displayFrame) {
        this.getBoardPanel().setDisplayFrame(displayFrame);
    }

    /**
     * Gets the board panel.
     *
     * @return the board panel
     */
    private BoardPanel getBoardPanel() {
        return this.boardPanel;
    }

    /**
     * Checks if is width looped.
     *
     * @return the boolean
     */
    public final Boolean isWidthLooped() {
        return this.getBoardPanel().isWidthLooped();
    }

    /**
     * Sets the width looped.
     *
     * @param widthLooped
     *            the new width looped
     */
    public final void setWidthLooped(final Boolean widthLooped) {
        this.getBoardPanel().setWidthLooped(widthLooped);
    }

    /**
     * Checks if is height looped.
     *
     * @return the boolean
     */
    public final Boolean isHeightLooped() {
        return this.getBoardPanel().isHeightLooped();
    }

    /**
     * Sets the height looped.
     *
     * @param heightLooped
     *            the new height looped
     */
    public final void setHeightLooped(final Boolean heightLooped) {
        this.getBoardPanel().setHeightLooped(heightLooped);
    }

    /**
     * Updates the boardPanel
     */
	@Override
	public void update() {
		boardPanel.update(null, null);
	}

    /**
     * Gets the dimension.
     * 
     */
	@Override
	public Dimension getDimension() {
		return this.dimension;
	}
}
