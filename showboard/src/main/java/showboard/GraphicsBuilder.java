package showboard;

//*<h1>The Interface IBoard.</h1>/*
public interface GraphicsBuilder {

	//* These methods get the : WidthLimit, HeightLimit, CornerMinX, CornerMaxX, CornerMinY, CornerMaxY, SquareSizeWidth, SquareSizeHeight/*
	int getWidthLimit();

	int getHeightLimit();

	int getCornerMinX();

	int getCornerMaxX();

	int getCornerMinY();

	int getCornerMaxY();

	int getSquareSizeWidth();

	int getSquareSizeHeight();

}