package showboard;

import java.awt.Graphics;
import java.awt.Image;

/**
 * <h1>The Interface ISquare.</h1>
 * <p>
 * A class can implement the ISquare interface when it wants to be show on a IBoard.
 * </p>
 *
 * @author Anne-Emilie DIET
 * @version 1.1
 * @see Image
 */
public interface Square {

    /**
     * Gets the image.
     * @param BoardFrameImpl 
     * @param x 
     * @param y 
     *
     * @return the image
     */
	Image getImage();
	
	void paintComponent(Graphics g);
}