package model;

import java.awt.Image;

/** 
<h1>The Interface Element.</h1>
 * @author Th�o De Ana
 * @version 1.0
 */
public interface Element {
	/**
	 * Gets the position.
	 * 
	 * @return position
	 */
	
	Position getPosition();
	
	/** 
	 * Loads the image
	 * 
	 * @param imageURL
	 */
	
	void loadImage(String imageURL);
	
	/** Gets the image
	 * 
	 * @return img
	 */
	Image getImage();
}
