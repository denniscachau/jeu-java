package model;

import java.sql.SQLException;

/**
 * <h1>The Interface Level.</h1>
 *
 * @author Jean-Aymeric DIET jadiet@cesi.fr
 * @version 1.0
 */
public interface Level {

    /**
     * Gets the example by id.
     *
     * @param id
     *            the id
     * @return the example by id
     * @throws SQLException
     *             the SQL exception
     */
	void manageEncounter(Character hero, Element gate, Element snake);
	void addElement(Element element);
	void removeElement(Element element);
	Element getElementByPosition(Position position);
	Character getLorann();
	void addLorann(Character character);
//	List<Character> getEnemies();
	
	void addObserver(LevelObserver observer);
	void notifyObserver();
}


