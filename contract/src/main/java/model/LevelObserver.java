package model;

/** 
<h1>The Interface LevelObserver.</h1>
 * @author Th�o De Ana
 * @version 1.0
 */
public interface LevelObserver {
	/**
	 * Notifies the view it needs to load something
	 */
	void onChange();
}
