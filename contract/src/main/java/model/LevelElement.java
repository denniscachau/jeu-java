package model;

/** 
<h1>The Enumration LevelElement.</h1>
 * @author Th�o De Ana
 * @version 1.0
 */
public enum LevelElement {

	HBone,
	VBone,
	Snake,
	GoldPocket,
	EnergyBubble,
	Spell,
	Hero,
	Enemy,
	Bone,
	Gate,
	Empty,
	
}
