/**
 * Provides all contracts for the model component.
 * 
 * @author Th�o De Ana
 * @version 1.0
 */
package model;
