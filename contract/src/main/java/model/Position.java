package model;

/** 
<h1>The Interface Position.</h1>
 * @author Th�o De Ana
 * @version 1.0
 */
public interface Position {
	/** Getters and setters */
	int getX();

	void setX(int x);

	int getY();

	void setY(int y);

}