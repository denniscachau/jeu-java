package model;

/** 
<h1>The Interface Character.</h1>
 * @author Th�o De Ana
 * @version 1.0
 */
public interface Character extends Element {
	/**
	 * Gets the direction.
	 *
	 * @return the direction
	 */
	Direction getDirection();
	
	/**
	 * Checks an encounter between 2 elements
	 */
	void encounter(LevelElement element);
	
	/**
	 * Checks if something has moved.
	 *
	 * @return a boolean
	 */
	boolean hasMoved();
	
	/**
	 * Sets something has moved
	 *
	 * @param moved
	 */
	void setMoved(boolean moved);
	
	/**
	 * Moves in a direction
	 * 
	 * @param direction
	 */
	public void move(Direction direction);
//	
//	public void moveUp() ;
//
//	public void moveRight() ;
//
//	public void moveDown() ;
//
//	public void moveLeft() ;
//
//	public void moveUpRight() ;
//
//	public void moveUpLeft() ;
//
//	public void moveDownRight() ;
//
//	public void moveDownLeft() ;
//
//	public void moveNothing();
}
