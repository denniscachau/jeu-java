package model;

/** 
<h1>The Enumration Direction.</h1>
 * @author Th�o De Ana
 * @version 1.0
 */
public enum Direction {
	
	UP,
	RIGHT,
	DOWN,
	LEFT,
	UPRIGHT,
	UPLEFT,
	DOWNRIGHT,
	DOWNLEFT,
	ACTION,
	NOTHING
}