package controller;

import java.sql.SQLException;

import model.Direction;
import view.ViewFacade;

/**
 * <h1>The Interface ControllerFacade.</h1>
 * 
 * @author Th�o De Ana
 * @version 1.0
 */
public interface ControllerFacade {
	void orderPerform(Direction direction);
	public void start() throws SQLException;
	public void setView(ViewFacade view);
}
