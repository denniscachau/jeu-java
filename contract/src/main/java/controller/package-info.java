/**
 * Provides all contracts for the controller component.
 *
 * @author Th�o De Ana
 * @version 1.0
 */
package controller;
