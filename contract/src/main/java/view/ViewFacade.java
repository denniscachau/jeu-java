package view;

import model.LevelObserver;

/**
 * <h1>The Interface ViewFacade.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public interface ViewFacade extends LevelObserver {

    /**
     * Display message.
     *
     * @param message
     *            the message
     */
    void displayMessage(String message);
}
