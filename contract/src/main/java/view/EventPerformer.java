package view;

import java.awt.event.KeyEvent;

/**
 * <h1>The Interface EventPerformer.</h1>
 *
 * @author Th�o De Ana
 * @version 1.0
 */
public interface EventPerformer {

	/** Performs an event
	 * @param keyEvent
	 */
	void eventPerform(KeyEvent keyEvent);

}