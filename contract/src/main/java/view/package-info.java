/**
 * Provides all contracts for the view component.
 *
 * @author Th�o De Ana
 * @version 1.0
 */
package view;
