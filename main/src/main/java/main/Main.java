package main;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.Level;
import model.dao.LevelDAO;

/**
 * <h1>The Class Main.</h1>
 *
 * @author Th�o De Ana, Lucas Suberbielle, Dennis Cachau, Vladimir Camalot
 * @version 1.0
 */
public abstract class Main {

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(final String[] args) {
    	
    	try {
    		
    	String selectLevel = JOptionPane.showInputDialog(null, "Quel niveau souhaitez-vous charger ?");
    	int levelSelecter = Integer.parseInt(selectLevel);
    	LevelDAO levelDAO = new LevelDAO();
    	Level level = levelDAO.getLevelById(levelSelecter);
    	System.out.println(level);
//    	ControllerFacade controller = new ControllerFacadeImpl(level);
//    	ViewFacade view = new ViewFacadeImpl(controller, level);
    	
//    	controller.setView(view);
//    	controller.start();
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

}
