package view;

import java.awt.event.KeyEvent;

import controller.ControllerFacade;
import model.Direction;

/**
 * <h1>The Class EventPerformerImpl.</h1>
 *
 * @author Vladimir Camalot & Lucas Suberbielle
 * @version 1.0
 */

public class EventPerformerImpl implements EventPerformer {
	ControllerFacade controllerFacade;
	Direction direction;
	
	/*
	* TO DO once basic directions are done :
	* public DiagonalHautDroite VK_UPRIGHT = VK_UP + VK_RIGHT;
	* public DiagonalHautGauche VK_UPRIGHT = VK_UP + VK_LEFT;
	* public DiagonalBasDroite VK_UPRIGHT = VK_DOWN + VK_RIGHT;
	* public DiagonalBasGauche VK_UPRIGHT = VK_DOWN + VK_LEFT;
	*/

	public EventPerformerImpl(ControllerFacade controllerFacade) {
		this.controllerFacade = controllerFacade;
	}

	/** 
	 * Processes the keyboard data
	 * 
	 * @param keyEvent
	 * 
	 * @return direction with the keyboard data
	 */
	@Override
	public void eventPerform(KeyEvent keyEvent) {
		int key = keyEvent.getKeyCode();

		switch (key) {
		case KeyEvent.VK_SPACE:
			//ControllerOrder.SPACE;
			break;

		case KeyEvent.VK_LEFT:
			direction = Direction.LEFT;
			break;

		case KeyEvent.VK_RIGHT:
			direction = Direction.RIGHT;
			break;

		case KeyEvent.VK_UP:
			direction = Direction.UP;
			break;

		case KeyEvent.VK_DOWN:
			direction = Direction.DOWN;
			break;

		default:
            System.out.println("Aucune touche");
            break;

		/*
		 * case KeyEvent.VK_UPRIGHT:
		 *	direction = Direction.UPRIGHT;
		 *	break;

		 * case KeyEvent.VK_UPLEFT:
		 *	direction = Direction.UPRIGHT;
		 *	break;

		 * case KeyEvent.VK_DOWNRIGHT:
		 *	direction = Direction.UPRIGHT;
		 *	break;

		 * case KeyEvent.VK_DOWNLEFT:
		 *	direction = Direction.UPRIGHT;
		 *	break;
			*/
		}
	}
}