package view;

import java.awt.Dimension;

import javax.swing.JOptionPane;

import controller.ControllerFacade;
import model.Element;
import model.Level;
import model.PositionImpl;
import showboard.BoardFrameImpl;
import showboard.GraphicsBuilder;
import showboard.Square;

/**
 * <h1>The Class ViewFacadeImpl provides a facade of the View component.</h1>
 *
 * @author Vladimir Camalot & Lucas Suberbielle
 * @version 1.0
 */
public class ViewFacadeImpl implements ViewFacade {

	private BoardFrameImpl boardFrame;
	GraphicsBuilder graphicsBuilder;
	EventPerformer eventPerformer;
	ControllerFacade controller;
	Level level;

	/**
	 * Instantiates a new view facade.
	 * 
	 * @param boardFrame
	 * @param graphicBuilder
	 * @param eventPerformer
	 * @param controller
	 */

	@SuppressWarnings("rawtypes")
	public ViewFacadeImpl(ControllerFacade controller, Level level) {
		eventPerformer = new EventPerformerImpl(controller);
		//graphicsBuilder = new GraphicsBuilder(level);
		boardFrame = new BoardFrameImpl("Lorann");
		boardFrame.setDimension(new Dimension(20, 12));
		for (int row = 0; row < 12; row++) {
			for (int col = 0; col < 20; col++) {
				Element modelElement = level.getElementByPosition(new PositionImpl(col, row));
				Square element = new ElementViewImpl(new PositionImpl(col, row), modelElement.getImage()); // modelElement
				boardFrame.addSquare(element, col, row);

			}
		}
		this.controller = controller;
//		level = model;
//		model = new LevelImpl();
	}

	/**
	 * Display a message
	 * 
	 * @param message
	 * 
	 */
	@Override
	public final void displayMessage(final String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	
	/**
	 * Close the frame
	 * 
	 */
	public void closeAll() {
		System.exit(0);

	}

	/**
	 * Call update method to reload the boardFrame
	 * 
	 */
	@Override
	public void onChange() {
		// Draw the game once again
		boardFrame.update();
	}

}
