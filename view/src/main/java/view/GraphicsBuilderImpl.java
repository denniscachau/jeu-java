package view;

import java.awt.Graphics;


import java.awt.image.ImageObserver;

import javax.swing.JFrame;

import model.Level;
import showboard.GraphicsBuilder;

/**
 * <h1>The Class GraphicsBuilderImpl provides the graphics necessary to build a level.</h1>
 *
 * @author Vladimir Camalot & Lucas Suberbielle
 * @version 1.0
 */
public class GraphicsBuilderImpl extends JFrame implements GraphicsBuilder {
	
	private static final long serialVersionUID = -603927884603654810L;
	
	/**
	 * The GraphicsBuilderImpl constructor
	 * 
	 * @param level
	 */

	public GraphicsBuilderImpl(Level level) {
		
	}
	
	
	/**
	 * Builds the level
	 * 
	 * @return the level
	 */
	public void buildLevel() {
		
	}
	
	/**
	 * Draw elements on the frame
	 * 
	 * @return elements
	 */
	public void drawElements(Character elements, Graphics graphics, ImageObserver observer) {
		
	}

	/**
	 * Gets the Width limit
	 * 
	 * @return the widthLimit
	 */
	@Override
	public int getWidthLimit() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Height limit
	 * 
	 * @return the heightLimit
	 */
	@Override
	public int getHeightLimit() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Corner Minimum of row
	 * 
	 * @return the cornerMinX
	 */
	@Override
	public int getCornerMinX() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Corner Maximum of row
	 * 
	 * @return the cornerMaxX
	 */
	@Override
	public int getCornerMaxX() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Corner Minimum of column
	 * 
	 * @return the cornerMinY
	 */
	@Override
	public int getCornerMinY() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Corner Maximum of column
	 * 
	 * @return the cornerMaxY
	 */
	@Override
	public int getCornerMaxY() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Suare Size Width
	 * 
	 * @return the squareSizeWidth
	 */
	@Override
	public int getSquareSizeWidth() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	
	/**
	 * Gets the Suare Size height
	 * 
	 * @return the squareSizeHeight
	 */
	@Override
	public int getSquareSizeHeight() {
		// TODO Auto-generated method stub
		
		return 0;
	}
	
}
